using System;
using System.ComponentModel.DataAnnotations;

namespace RazorPages.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double GPA { get; set; }
        [Display(Name = "Group number")]
        public int Group_number {get; set;}
    }
}